import java.util.Arrays;
import java.lang.Math;

import edu.rit.util.Packing;
import edu.rit.util.Hex;

public class cmac implements Mac{
	
	private BlockCipher cipher;
	private int Tlen; // CMAC tag length in bytes
	private int Mlen; // message length in bytes
	
	private byte[] key;
	private long k1;
	private long k2;
	
	private int n; // number of blocks
	private byte[] prev_digest;
	private byte[] message;
	private int hash_counter;
	private final long Rb = 0x1BL;
	
	/**
	 * cmac constructor
	 *
	 * @param  	cipher 	-- 	Block cipher
	 * @param	Mlen	--	Message length in bytes
	 * @param	Tlen	--	Tag length in bytes
	 *
	 */
	public cmac(BlockCipher cipher, int Mlen){
		this.cipher = cipher;
		this.Tlen = cipher.blockSize();
		this.Mlen = Mlen;
		
		this.message = new byte[Mlen];
		
		// If Mlen = 0, let n = 1
		if(this.Mlen == 0){
			this.n = 1;
		}
		// else, let n = [Mlen/b]
		else{
			double mlen_t = this.Mlen;
			double tlen_t = this.Tlen;
			this.n =  (int)Math.ceil(mlen_t/tlen_t);
		}
		this.prev_digest = Hex.toByteArray("0000000000000000");
	}
	
	/**
	 * Returns this MAC's digest size in bytes.
	 *
	 * @return  Digest size.
	 */
	public int digestSize(){
		return this.Tlen;
	}

	/**
	 * Returns this MAC's key size in bytes.
	 *
	 * @return  Key size.
	 */
	public int keySize(){
		return this.cipher.keySize();
	}

	/**
	 * Set the key for this MAC. <TT>key</TT> must be an array of bytes whose
	 * length is equal to <TT>keySize()</TT>.
	 *
	 * @param  key  Key.
	 */
	public void setKey(byte[] key){
		if(key.length == keySize()){
			this.key = key;
			this.cipher.setKey(key);
			subKeySetUp();
		}
		else{
			System.err.printf("Incorrect key size! \n");
		}
	}
	
	/**
	* Create subkeys K1, K2
	*/
	public void subKeySetUp(){
		// Let L = CIPHK(0b)
		byte[] zeros = Hex.toByteArray("0000000000000000");
		cipher.encrypt(zeros);		
		long L = Packing.packLongBigEndian(zeros, 0);
		
		// If MSB(L) = 0, then K1 = L << 1;
		int msb_L = (int)((L & 0xf000000000000000L) >> 60);
		msb_L = (msb_L & 0xf) >> 3;
		if(msb_L == 0){
			this.k1 = L << 1;
		}		
		// Else K1 = (L << 1) xor Rb;
		else{
			this.k1 = (L << 1) ^ this.Rb;
		}
		
		// If MSB(K1) = 0, then K2 = K1 << 1;
		int msb_K1 = (int)((this.k1 & 0xf000000000000000L) >> 60);
		msb_K1 = (msb_K1 & 0xf) >> 3;
		if(msb_K1 == 0){
			this.k2 = this.k1 << 1;
		}
		// Else K2 = (K1 << 1) xor Rb
		else{
			this.k2 = (this.k1 << 1) ^ this.Rb;
		}	
	}
	
	/**
	 * Append the given byte to the message being authenticated. Only the least
	 * significant 8 bits of <TT>b</TT> are used.
	 *
	 * @param  b  Message byte.
	 */
	public void hash(int b){
		try{
			this.message[this.hash_counter++] = (byte)(b&0xff);
		}
		catch(ArrayIndexOutOfBoundsException ex){
			System.err.printf("Message array overflow! \n");
		}		
	}

	/**
	 * Obtain the message digest. <TT>d</TT> must be an array of bytes whose
	 * length is equal to <TT>digestSize()</TT>. The message consists of the
	 * series of bytes provided to the <TT>hash()</TT> method. The digest of the
	 * message is stored in the <TT>d</TT> array.
	 *
	 * @param  digest  Message digest (output).
	 */
	public void digest(byte[] d){	
		if(d.length != digestSize()){
			System.err.printf("Incorrect parameter size! \n");
			return;
		}
		
		byte[] current_block = new byte[8];
		
		// for each block in the message
		for(int i=0; i<this.n; i++){
			int index = i * 8;
			current_block = Arrays.copyOfRange(this.message,index,index+8);
			
			// if last block
			if(i+1 == this.n){
				// check if the last block needs padding
				if(this.Mlen % this.Tlen != 0){
					current_block[this.Mlen % this.Tlen] = (byte)0x80;
					
					// convert K2 long -> byte array
					byte[] k2_byteArray = Hex.toByteArray("0000000000000000");
					Packing.unpackLongBigEndian(this.k2, k2_byteArray, 0);
					
					// xor with K2
					for(int j=0; j<current_block.length; j++){
						current_block[j] ^= k2_byteArray[j];
					}
				}
				else{
					// convert K1 long -> byte array
					byte[] k1_byteArray = Hex.toByteArray("0000000000000000");
					Packing.unpackLongBigEndian(this.k1, k1_byteArray, 0);
					
					// xor with K1
					for(int j=0; j<current_block.length; j++){
						current_block[j] ^= k1_byteArray[j];
					}
				}			
			}
			
			// xor with previous digest
			for(int j=0; j<8; j++){
				current_block[j] ^= prev_digest[j];
			}
			
			// encrypt the block
			cipher.encrypt(current_block);
			
			// save the result for the next iteration
			for(int j=0; j<8; j++){
				prev_digest[j] = current_block[j];
			}
		}
		
		for(int j=0; j<current_block.length; j++){
			// d <= current_block
			d[j] ^= current_block[j];
			
			// prev_digest <= 0
			prev_digest[j] = (byte)0x00;
		}
		
		// reset the message array index
		this.hash_counter = 0;
	}
}
