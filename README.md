# CMAC and MacGuffin README

CMAC Specification:
http://csrc.nist.gov/publications/nistpubs/800-38B/SP_800-38B.pdf

MacGuffin Specification:
https://www.schneier.com/paper-macguffin.pdf

This project uses the PJ2 library. 

PJ2 executable distribution:
http://www.cs.rit.edu/~ark/pj2.shtml#download
http://www.cs.rit.edu/~ark/pj2_20131202.jar

rename pj2_20131202.jar -> /lib/pj2.jar

Compile:
javac -cp "lib/pj2.jar;." TimeMac.java

Run:
java -cp "lib/pj2.jar;." TimeMac