import edu.rit.util.Hex;

public class TimeMac {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		byte[] key = Hex.toByteArray("00000000000000000000000000000000");
		byte[] digest = null;	
		int message_size = 16;
		
		BlockCipher cipher = new macguffin();								
		Mac mac = new cmac(cipher, message_size);									
		mac.setKey(key);													
		
		long startTime = System.nanoTime();
		for(int i = 0; i < 1; i++){
			for(int j=0; j<message_size; j++){
				mac.hash(00);
			}
			digest = Hex.toByteArray("0000000000000000");
			mac.digest(digest);
		}
		
		long endTime = System.nanoTime();
		long duration = endTime - startTime;
		System.out.printf("Duration: %d \n", duration);
		System.out.printf("Digest: %s \n",Hex.toString(digest));		
	}
}
