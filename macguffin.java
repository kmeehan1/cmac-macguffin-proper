import java.util.Arrays;
import java.nio.ByteBuffer;

import edu.rit.util.Packing;
import edu.rit.util.Hex;

public class macguffin implements BlockCipher{
	private byte[] ku; // Key bits 79..16 stored in keyupper bits 63..0.
	private byte[] kl; // Key bits 15..0 stored in keylower bits 63..48.
	
	// S-Boxes S1-S8
	private static int[] s1 = {2,0,0,3,3,1,1,0,0,2,3,0,3,3,2,1,1,2,2,0,0,2,2,3,1,3,3,1,0,1,1,2,
		0,3,1,2,2,2,2,0,3,0,0,3,0,1,3,1,3,1,2,3,3,1,1,2,1,2,2,0,1,0,0,3};
		
	private static int[] s2 = {3,1,1,3,2,0,2,1,0,3,3,0,1,2,0,2,3,2,1,0,0,1,3,2,2,0,0,3,1,3,2,1,
		0,3,2,2,1,2,3,1,2,1,0,3,3,0,1,0,1,3,2,0,2,1,0,2,3,0,1,1,0,2,3,3};

	private static int[] s3 = {2,3,0,1,3,0,2,3,0,1,1,0,3,0,1,2,1,0,3,2,2,1,1,2,3,2,0,3,0,3,2,1,
		3,1,0,2,0,3,3,0,2,0,3,3,1,2,0,1,3,0,1,3,0,2,2,1,1,3,2,1,2,0,1,2};
		
	private static int[] s4 = {1,3,3,2,2,3,1,1,0,0,0,3,3,0,2,1,1,0,0,1,2,0,1,2,3,1,2,2,0,2,3,3,
		2,1,0,3,3,0,0,0,2,2,3,1,1,3,3,2,3,3,1,0,1,1,2,3,1,2,0,1,2,0,0,2};
		
	private static int[] s5 = {0,2,2,3,0,0,1,2,1,0,2,1,3,3,0,1,2,1,1,0,1,3,3,2,3,1,0,3,2,2,3,0,
		0,3,0,2,1,2,3,1,2,1,3,2,1,0,2,3,3,0,3,3,2,0,1,3,0,2,1,0,0,1,2,1};
		
	private static int[] s6 = {2,2,1,3,2,0,3,0,3,1,0,2,0,3,2,1,0,0,3,1,1,3,0,2,2,0,1,3,1,1,3,2,
		3,0,2,1,3,0,1,2,0,3,2,1,2,3,1,2,1,3,0,2,0,1,2,1,1,0,3,0,3,2,0,3};
		
	private static int[] s7 = {0,3,3,0,0,3,2,1,3,0,0,3,2,1,3,2,1,2,2,1,3,1,1,2,1,0,2,3,0,2,1,0,
		1,0,0,3,3,3,3,2,2,1,1,0,1,2,2,1,2,3,3,1,0,0,2,3,0,2,1,0,3,1,0,2};
		
	private static int[] s8 = {3,1,0,3,2,3,0,2,0,2,3,1,3,1,1,0,2,2,3,1,1,0,2,3,1,0,0,2,2,3,1,0,
		1,0,3,1,0,2,1,1,3,0,2,2,2,2,0,3,0,3,0,2,2,3,3,0,3,1,1,1,1,0,2,3};
	
	//List that stores all of the sboxes
	private static int[][] sbox = {s1,s2,s3,s4,s5,s6,s7,s8};
	
	//List that stores the three round keys for all 32 rounds
	private short[][] roundKeys = new short[32][3];

	/**
	 * Returns this block cipher's block size in bytes.
	 *
	 * @return  Block size.
	 */
	public int blockSize(){
		return 8; //Block size of 8 bytes
	}

	/**
	 * Returns this block cipher's key size in bytes.
	 *
	 * @return  Key size.
	 */
	public int keySize(){
		return 16; //Keysize is 16 Bytes
	}

	/**
	 * Set the key for this block cipher. <TT>key</TT> must be an array of bytes
	 * whose length is equal to <TT>keySize()</TT>.
	 *
	 * @param  key  Key.
	 */
	public void setKey(byte[] key){
		this.ku = Arrays.copyOfRange(key,0,8); //Pack the upper 8 bytes into key lower
		this.kl = Arrays.copyOfRange(key,8,16); // Pack the lower 8 bytes into key upper
		key_setup(key);
	}
	
	
	/**
	 * Generate the round keys. <TT>key</TT> must be an array of bytes
	 * whose length is equal to <TT>keysize()</TT>. On input, <TT>key</TT>
	 * contains the 128 bit key. The key is broken into keyupper(ku) and keylower(kl)
	 * where keyupper contains the 8 most significant bytes and keylower contains the 
	 * 8 least significant bytes.
	 *
	 * @param  key  Byte array containing the inputted key
	 */
	public void key_setup(byte[] key){
		
		for(int h=0; h<32; h++){
			encrypt(kl);
			roundKeys[h][0] = ByteBuffer.wrap(Arrays.copyOfRange(kl,0,2)).getShort();
			roundKeys[h][1] = ByteBuffer.wrap(Arrays.copyOfRange(kl,2,4)).getShort();
			roundKeys[h][2] = ByteBuffer.wrap(Arrays.copyOfRange(kl,4,6)).getShort();
		}
		
		for(int h=0; h<32; h++){
			encrypt(ku);
			roundKeys[h][0] ^= ByteBuffer.wrap(Arrays.copyOfRange(ku,0,2)).getShort();
			roundKeys[h][1] ^= ByteBuffer.wrap(Arrays.copyOfRange(ku,2,4)).getShort();
			roundKeys[h][2] ^= ByteBuffer.wrap(Arrays.copyOfRange(ku,4,6)).getShort();
		}		
	}

	/**
	 * Encrypt the given plaintext. <TT>text</TT> must be an array of bytes
	 * whose length is equal to <TT>blockSize()</TT>. On input, <TT>text</TT>
	 * contains the plaintext block. The plaintext block is encrypted using the
	 * key specified in the most recent call to <TT>setKey()</TT>. On output,
	 * <TT>text</TT> contains the ciphertext block.
	 *
	 * @param  text  Plaintext (on input), ciphertext (on output).
	 */
	public void encrypt(byte[] text){
		
		//Pack the message into 16 bit blocks
		short left = Packing.packShortBigEndian(text, 0);
		short a = Packing.packShortBigEndian(text, 2);
		short b = Packing.packShortBigEndian(text, 4);
		short c = Packing.packShortBigEndian(text, 6);
		
		byte s_index;
		short t;
		short temp;
		long data = 0;
		
		//32 Rounds of Encryption
		for(int i=0; i<32; i++){
			t = 0;
			
			//Build up of the 16bit t variable
			for(int j=1; j<9; j++){
				s_index = 0;
				
				a ^= roundKeys[i][0]; 
				b ^= roundKeys[i][1]; 
				c ^= roundKeys[i][2];
				
				//Decide which Sbox we should map the input to
				switch(j){
					case 1: 
						s_index |= ((a>>2)&0x01)<<5;
						s_index |= ((a>>5)&0x01)<<4;
						s_index |= ((b>>6)&0x01)<<3;
						s_index |= ((b>>9)&0x01)<<2;
						s_index |= ((c>>11)&0x01)<<1;
						s_index |= ((c>>13)&0x01);
						t |= sbox[j-1][s_index];
						break;
					case 2: 
						s_index |= ((a>>1)&0x01)<<5;
						s_index |= ((a>>4)&0x01)<<4;
						s_index |= ((b>>7)&0x01)<<3;
						s_index |= ((b>>10)&0x01)<<2;
						s_index |= ((c>>8)&0x01)<<1;
						s_index |= ((c>>14)&0x01);
						t |= (sbox[j-1][s_index])<<2;
						break;
					case 3: 
						s_index |= ((a>>3)&0x01)<<5;
						s_index |= ((a>>6)&0x01)<<4;
						s_index |= ((b>>8)&0x01)<<3;
						s_index |= ((b>>13)&0x01)<<2;
						s_index |= ((c>>0)&0x01)<<1;
						s_index |= ((c>>15)&0x01);
						t |= (sbox[j-1][s_index])<<4;
						break;
					case 4: 
						s_index |= ((a>>12)&0x01)<<5;
						s_index |= ((a>>14)&0x01)<<4;
						s_index |= ((b>>1)&0x01)<<3;
						s_index |= ((b>>2)&0x01)<<2;
						s_index |= ((c>>4)&0x01)<<1;
						s_index |= ((c>>10)&0x01);
						t |= (sbox[j-1][s_index])<<6;
						break;
					case 5: 
						s_index |= ((a>>0)&0x01)<<5;
						s_index |= ((a>>10)&0x01)<<4;
						s_index |= ((b>>3)&0x01)<<3;
						s_index |= ((b>>14)&0x01)<<2;
						s_index |= ((c>>6)&0x01)<<1;
						s_index |= ((c>>12)&0x01);
						t |= (sbox[j-1][s_index])<<8;
						break;
					case 6: 
						s_index |= ((a>>7)&0x01)<<5;
						s_index |= ((a>>8)&0x01)<<4;
						s_index |= ((b>>12)&0x01)<<3;
						s_index |= ((b>>15)&0x01)<<2;
						s_index |= ((c>>1)&0x01)<<1;
						s_index |= ((c>>5)&0x01);
						t |= (sbox[j-1][s_index])<<10;
						break;
					case 7: 
						s_index |= ((a>>9)&0x01)<<5;
						s_index |= ((a>>15)&0x01)<<4;
						s_index |= ((b>>5)&0x01)<<3;
						s_index |= ((b>>11)&0x01)<<2;
						s_index |= ((c>>2)&0x01)<<1;
						s_index |= ((c>>7)&0x01);
						t |= (sbox[j-1][s_index])<<12;
						break;
					case 8: 
						s_index |= ((a>>11)&0x01)<<5;
						s_index |= ((a>>13)&0x01)<<4;
						s_index |= ((b>>0)&0x01)<<3;
						s_index |= ((b>>4)&0x01)<<2;
						s_index |= ((c>>3)&0x01)<<1;
						s_index |= ((c>>9)&0x01);
						t |= (sbox[j-1][s_index])<<14;
						break;
					default :
						System.out.printf("Case statement error! \n");
						break;
				}
			}
			
			//Xor the lookup to the left block
			left ^= t;
			
			//Left rotate the blocks
			temp = left;
			left = a;
			a = b;
			b = c;
			c = temp;			
		}
		
		//combine the blocks in array data and pack them into text
		data |= (((long)left)<<48)&0xffff000000000000L;
		data |= (((long)a)<<32)&0xffff00000000L;
		data |= (((long)b)<<16)&0xffff0000L;
		data |= c&0xffff;		
		Packing.unpackLongBigEndian(data, text, 0);
	}
	
	public static void main(String args[]){	
		BlockCipher cipher = new macguffin();
		byte[] key = Hex.toByteArray("00000000000000000000000000000000");
		byte[] text = Hex.toByteArray("0000000000000000");
		System.out.printf("%s \n",Hex.toString(text));
		cipher.setKey(key);
		System.out.printf("encrypt starting now \n");
		cipher.encrypt(text);		
		System.out.printf("%s \n",Hex.toString(text));
		
	}

}
