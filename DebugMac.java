import edu.rit.util.Hex;

public class DebugMac {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		byte[] key = Hex.toByteArray("00000000000000000000000000000000");	
		BlockCipher cipher = new macguffin();								
		Mac mac = new cmac(cipher, 16);										
		mac.setKey(key);	
		for(int i=0; i<16; i++){
			mac.hash(17);
		}
		byte[] text = Hex.toByteArray("0000000000000000");
		mac.digest(text);
		System.out.printf("digest: %s \n",Hex.toString(text));
	}

}
